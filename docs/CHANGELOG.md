# Changelog

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/compare/release/1.2.1...release/1.3.0) (2025-01-06)


### Features

* update appVersion to 26.0.4-eole3.0 ([c180960](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/c180960e72c53594f26f529a6b7135451abc7f87))

### [1.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/compare/release/1.2.0...release/1.2.1) (2024-05-30)


### Bug Fixes

* update app version to v24.4.10-eole3.0 ([9da8b69](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/9da8b696ac1f04fc1c496d9df335ab61e42744da))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-05-30)


### Features

* deploy image v24.4.10 from harbor ([45d5b3c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/45d5b3cd99cafb5cb29bdfcb19f9ff017dcd887b))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/compare/release/1.0.0...release/1.1.0) (2023-12-18)


### Features

* update appversion to 22.1.11 ([b26ad8a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/b26ad8a0cd2a60c041dab0e59d9ee927096f066b))

## 1.0.0 (2023-12-04)


### Features

* change appversion to 21.5.1 for drawio ([455e995](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/455e9950472deefcdd1f9b63cac3d684a2f96627))
* first release for draw ([9ab9ed4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/9ab9ed435c4f61a7fdd24548d16f171b0e88c833))
* name is drawio and using latest version ([945a993](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/945a9939f597b6a4364834d152bdd76377328f16))
* update appversion to 20.8.20 ([5f45536](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/5f45536c1b2d258424bb159ee765a74cc96879b1))
* update appversion to 21.0.2 ([c44cf14](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/c44cf14dad41c625e3577a267170c08e315683d4))
* update appversion to 21.6.8 ([06a9645](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/06a96452701285c32993ef2805c3d6146da93c25))
* update to appversion 22.0.3 ([c044ad7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/c044ad72234c9c46e378b0e6e933404f3997822c))


### Bug Fixes

* gitlab-ci stable branch is main ([2b0b0c5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/2b0b0c59419bb16e565af839a2babe6797663539))
* stable branch in releaserc.js ([c426778](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/c4267784da2e87918695170b40a534b76d1b1b4d))
* update app version to 21.3.0 ([5016cfe](https://gitlab.mim-libre.fr/EOLE/eole-3/services/drawio/drawio-helm-chart/commit/5016cfe115b888294616053a1c48ab6b4f32f7d2))
